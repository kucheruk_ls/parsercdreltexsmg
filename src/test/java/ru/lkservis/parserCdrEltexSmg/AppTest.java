package ru.lkservis.parserCdrEltexSmg;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Unit test for simple App.
 */
public class AppTest {

    public static void main(String[] args) {
        CdrParser cdrParser = new CdrParser(Paths.get(args[0]));

        for (Path p : cdrParser.getFileDirList()) {
            System.out.println(p.toString());
        }
    }

    /**
     * Rigorous Test :-)
     */
    @Test
    public void shouldAnswerWithTrue()
    {
        assertTrue( true );
    }
}

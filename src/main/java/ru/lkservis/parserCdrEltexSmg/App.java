package ru.lkservis.parserCdrEltexSmg;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Hello world!
 *
 */
public class App{
    public static void main( String[] args ){

        LogAndFileCleaner logAndFileCleaner = new LogAndFileCleaner();
        //почистим лог если он больше 5 Мб
        logAndFileCleaner.logPack();
        //удалим архивы с истекшим сроком хранения



        System.out.println("конфиг файл:"+args[0]);
        CdrParser cdrParser = new CdrParser(Paths.get(args[0])); //создаем парсер и указываем конфигурационный файл в качестве параметра
        MyLogger myLogger = new MyLogger(App.class);
        myLogger.print("Парсер создан, файл конфигурации принят!");
        myLogger.print("выведем строки лога: ");
        for(CdrString cs:cdrParser.getListCdrString()){
            myLogger.print(cs.toString());
        }

        myLogger.print("Посмотрим работу метода отборщика звонков:");

        for(Map.Entry<String, ArrayList<String>> entry:cdrParser.createCalledList().entrySet()){
            myLogger.print("внутренний: "+entry.getKey());
            for(String s:entry.getValue()){
                myLogger.print(s);
            }
        }
        ReportCreator reportCreator = new ReportCreator(cdrParser.createCalledList());
        reportCreator.reportListGenerator();
        myLogger.print("Отчеты сформированы");
        //отчеты сформировали

        // будем их паковать!
        ZipArchiver zipArchiver = new ZipArchiver(cdrParser);
        Path zip = null;
        try{
            zip = zipArchiver.createZip(Paths.get(ConfigReader.getReportDir()));
            myLogger.print("Zip архив создан!");
        }catch (Exception e){
            myLogger.print("Ошибка упаковки файлов");
            myLogger.printStackElements(e.getStackTrace());
        }
        myLogger.print("Будем отправлять:");
        ZipSendMail zipSendMail = new ZipSendMail();
        zipSendMail.send(zip.toString(),zip.getFileName().toString());
        myLogger.print("Архив успешно отправлен!");

        cdrParser.prefixMarker();

    }
}

package ru.lkservis.parserCdrEltexSmg;

import java.io.*;
import java.nio.file.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import static java.nio.file.StandardCopyOption.ATOMIC_MOVE;

public class CdrParser {
    private Path cdrDir;
    private MyLogger myLogger = new MyLogger(CdrParser.class);
    private ArrayList<Path> fileDirList;
    private ArrayList<CdrString> listCdrString;
    private ConfigReader configReader;


    public CdrParser(Path cfgDir) {
        listCdrString = new ArrayList<>();
        try{
            configReader = new ConfigReader(cfgDir.toFile());
            myLogger.print("прочитал файл конфигурации "+cfgDir.toFile());
        }catch (IOException e){
            myLogger.printStackElements(e.getStackTrace());
        }
        //String cdrD = configReader.getCdrDir();
        this.cdrDir = Paths.get(configReader.getCdrDir());
        ArrayList<Path> fileDirList = new ArrayList<>();
        try (DirectoryStream<Path> directoryStream = Files.newDirectoryStream(this.cdrDir)) {//обходим дирректорию и получаем список файлов
            myLogger.print("Обходим директорию и смотрим, что там за файлы: ");
            for (Path path : directoryStream) {
                myLogger.print(path.toString());
                if (path.getFileName().toString().endsWith(".cdr")&&!path.getFileName().toString().startsWith(ConfigReader.getPrefixForProcFile())) {
                    fileDirList.add(path);//сохраним список файлов в лист
                }
                if(path.getFileName().toString().startsWith(ConfigReader.getPrefixForProcFile())&&isOldFileForDelete(path)==true){
                    try{
                        Files.delete(path);
                        myLogger.print("Файл "+path+" успешно удален!");
                    }catch (IOException e){
                        myLogger.print("Ошибка удаления файла "+path);
                        myLogger.printStackElements(e.getStackTrace());
                    }
                }
            }
            myLogger.print("Закончили обход рабочей директории.");
            this.fileDirList = fileDirList;
        } catch (IOException e) {
            myLogger.printStackElements(e.getStackTrace());
        }

        //будем обрабатывать файлы
        myLogger.print("Список файлов к обработке:");
        for(Path p:fileDirList){
            myLogger.print(p.toString());
        }

        for(Path p:getFileDirList()){
            BufferedReader bufferedReader = null;
            try{
                bufferedReader = new BufferedReader(new FileReader(p.toFile()));
                String s = null;
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                while ((s=bufferedReader.readLine())!=null){
                    String[] tmp = s.split(";");
                    String indPriznak = tmp[0];//отличительный признак
                    Date tmpD = new Date();
                    tmpD = simpleDateFormat.parse(tmp[1]);
                    Date responseTime = tmpD;//время ответа на вызов
                    double callDuration = Double.parseDouble(tmp[2]);//длительность вызова
                    String reasonDisconnect = tmp[3];//причина зазъединения
                    String statusCallDisconnect = tmp[4];//статус вызова при разъединении
                    String callerIp = tmp[5];//ip адрес вызывающего
                    String callerTipe = tmp[6];//тип вызывающего
                    String callerSubscribe = tmp[7];//описание вызывающего
                    String callerInputNumber = tmp[8];//входящий номер вызывающего
                    String callerOutputNumber = tmp[9];//исходящий номер вызывающего
                    String calledIp = tmp[10];//ip адрес вызываемого
                    String calledTipe = tmp[11];//тип вызываемого
                    String calledSubscribe = tmp[12];//описание вызываемого
                    String calledInputNumber = tmp[13];//входящий номер вызываемого
                    String calledOutputNumber = tmp[14];//исходящий номер вызываемого
                    Date tmpD2 = new Date();
                    tmpD2 = simpleDateFormat.parse(tmp[15]);
                    Date callArrivalTime = tmpD2;//время поступления вызова
                    Date tmpD3 = new Date();
                    tmpD3 = simpleDateFormat.parse(tmp[16]);
                    Date callDisconnectTime = tmpD3;//время разъединения вызова
                    //CdrString d = new CdrString()
                    CdrString cdrString = new CdrString(indPriznak,responseTime,callDuration,reasonDisconnect,statusCallDisconnect,callerIp,callerTipe,callerSubscribe,callerInputNumber,callerOutputNumber,calledIp,calledTipe,calledSubscribe,calledInputNumber,calledOutputNumber,callArrivalTime,callDisconnectTime);
                    this.listCdrString.add(cdrString);

                }
                bufferedReader.close();
            }catch (FileNotFoundException e){
                myLogger.printStackElements(e.getStackTrace());
            }catch (IOException e){
                myLogger.printStackElements(e.getStackTrace());
            }catch (ParseException e){
                myLogger.printStackElements(e.getStackTrace());
            }finally {
                try{
                    bufferedReader.close();
                }catch (IOException e){
                    myLogger.print("Не удалось аварийно закрыть поток чрения cdr файлов");
                    myLogger.printStackElements(e.getStackTrace());
                }
            }
        }



    }

    /**
     * класс обрабатывает список вызовов и возвращает вызовы с внутренних номеров на междугородние в виде Map где ключ
     * внутренний номер абонента а значение строка с информацией о исходящем вызове на номера вида 8x+; или 7x+;
     * callerOutputNumber - внутренний звонящего
     * calledOutputNumber - исходящий межгород и т.д.
     * формат строки внутреннего списка [дата] [время] [исходящийНомер] [продолжительностьЗвонка] [адресат либо прочерк]
     * @return
     */
    public HashMap<String , ArrayList<String>> createCalledList(){
        ArrayList<CdrString> parseListCdr = getListCdrString();
        HashMap<String,ArrayList<String>> returnList = new HashMap<>();
        for(CdrString ss:parseListCdr){
            boolean ustConnect = (ss.getResponseTime()!=ss.getCallDisconnectTime());
            if(ss.getCalledOutputNumber().startsWith("8")&&ss.getCalledOutputNumber().length()>10&&ustConnect){
                String stringCallerOutputNumber = ""; // заготовим пустую строку для номера звонящего
                if(ss.getCallerOutputNumber().length()<1||ss.getCallerOutputNumber()==null){ // если номера нет будем использовать описание
                    stringCallerOutputNumber = ss.getCallerSubscribe();
                }else{
                    stringCallerOutputNumber = ss.getCallerOutputNumber();
                }
                if(returnList.containsKey(stringCallerOutputNumber)){
                    ArrayList<String> tmp = returnList.get(stringCallerOutputNumber);
                    tmp.add(ReportStringCreator(ss));
                    returnList.put(stringCallerOutputNumber,tmp);
                }else{
                    ArrayList<String> tmp = new ArrayList<>();
                    tmp.add(zagolovokStringCreator(ss));
                    tmp.add(ReportStringCreator(ss));
                    returnList.put(stringCallerOutputNumber,tmp);
                }
            }
        }
        return returnList;
    }

    public Path getCdrDir() {
        return cdrDir;
    }

    public ArrayList<Path> getFileDirList() {
        return fileDirList;
    }

    public ArrayList<CdrString> getListCdrString() {
        return listCdrString;
    }

    /**
     * метод получает в качестве параметра строку Cdr файла выбирает из нее необходимые данные:
     * формат строки внутреннего списка        [датаВремя] [исходящийНомер] [продолжительностьЗвонка] [адресат либо прочерк]
     * длина полей строки(или ширина столбцов) 20           17               6                         40
     *
     * и возвращает эту строку
     * @param ss - строка Cdr файла в виде объекта класса CdrString
     * @return - Строку окончательного вида для отчета о звонках
     */
    private String ReportStringCreator(CdrString ss){
        String returnString = "";
        StringBuilder sb = new StringBuilder();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String strResponseTime = simpleDateFormat.format(ss.getResponseTime());
        sb.append(dopolnyashkaProbelyashka(strResponseTime,21));
        sb.append(dopolnyashkaProbelyashka(ss.getCalledOutputNumber(),17));
        String callDuration = String.format("%.2f",(ss.getCallDuration()/60));
        sb.append(dopolnyashkaProbelyashka(callDuration,9));
        String stringCalledOutputNumber = ss.getCalledOutputNumber();
        HashMap<String,String> sprOutCallMap = SprOutCall.getSprOutCall();
        boolean containCON = sprOutCallMap.containsKey(stringCalledOutputNumber);
        if(containCON){
            sb.append(dopolnyashkaProbelyashka(SprOutCall.getSprOutCall().get(ss.getCalledOutputNumber()),40));
        }else {
            sb.append(dopolnyashkaProbelyashka("_____________________________",40));
        }
        returnString = sb.toString();
        return returnString;
    }

    private String zagolovokStringCreator(CdrString ss){
        String returnString = "";
        StringBuilder sb = new StringBuilder();
        String strResponseTime = "Дата и время звонка";
        sb.append(dopolnyashkaProbelyashka(strResponseTime,21));
        sb.append(dopolnyashkaProbelyashka("Исходящий номер",17));
        sb.append(dopolnyashkaProbelyashka("мин.сек.",9));
        sb.append(dopolnyashkaProbelyashka("Адресат звонка",40));

        returnString = sb.toString();
        return returnString;
    }

    /**
     * метод получает строку s и возвращает ее равной длине указанной в параметре i обрезая или дополняя пробелами
     * @param s - строка которую нужно(если нужно дополнить пробелами)
     * @param i - длина строки результата
     * @return
     */
    public String dopolnyashkaProbelyashka(String s, int i){
        StringBuilder sb = new StringBuilder();
        if(s.length()>=i){
            sb.append(s.substring(0,i-1));
            sb.append(" ");
        }else{
            sb.append(s);
            while (sb.length()!=i){
                sb.append(" ");
            }
        }
        return sb.toString();
    }

    public void prefixMarker(){
        if(getFileDirList().size()==0){
            myLogger.print("Нет файлов для разбора и отправки: ");
        }else {
            myLogger.print("Обработка следующих файлов завершена(файлы помечаются префиксом): ");
            ArrayList<Path> list = getFileDirList();
            for (Path p : list) {
                myLogger.print(p.toString());
                Path pr = p.resolveSibling(ConfigReader.getPrefixForProcFile() + p.getFileName());
                try {
                    Files.move(p, pr, ATOMIC_MOVE);
                    myLogger.print("Файл " + p + " успешно переименован");
                } catch (IOException e) {
                    myLogger.print("Ошибка переименования файла " + p);
                }

//                File files = p.toFile();
//                File filet = new File(files.getParent()+"\\"+configReader.getPrefixForProcFile()+files.getName());
//                try{
//                    files.renameTo(filet);
//                }catch (Exception e){
//                        myLogger.print("Ошибка переименования файла "+p);
//                }
//                files.renameTo(filet);
//                myLogger.print("файл "+files.getName()+" переименован в "+filet.getName());
            }
        }
    }

    public boolean isOldFileForDelete(Path path) {
        myLogger.print("Вызван метод isOldFileDelete, если файл "+path+" был последний раз модифицирован более savetimeday(смотри config.xml) назад он будет удален.");
        File fls = path.toFile();
        long lmfile = fls.lastModified();
        long tekDay = new Date().getTime();
        long raznicaDat = tekDay - lmfile;
        long lastModDay = (raznicaDat / 1000 / 60 / 60 / 24);

        int delParam = ConfigReader.getSaveTimeDay();
        if (lastModDay >= delParam) {
            return true;
        }else{
            return false;
        }

    }
}

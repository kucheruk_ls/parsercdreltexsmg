package ru.lkservis.parserCdrEltexSmg;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.*;
import javax.mail.internet.*;
import java.util.Properties;

public class ZipSendMail {

    /**
     *
     * @param zipFilePath - полный путь с именем файла
     * @param zipFileName - имя файла
     */
    public static void send(String zipFilePath, String zipFileName){
        String to1 = ConfigReader.getReceiverMail(); //адресат
        //String to2 = ConfigReader.getReceiverMail2(); //адресат 2
        String[] massRessiveAddr = ConfigReader.getReceiverMail().split(",");
        for(String s:massRessiveAddr){
            System.out.println("отладочная печать адреса: "+s);
        }
        Address[] receiverList = new InternetAddress[massRessiveAddr.length];
        for(int i = 0;i<massRessiveAddr.length;i++){
            try{
                receiverList[i] = new InternetAddress(massRessiveAddr[i]);
            }catch (AddressException e){
                e.printStackTrace();
            }
        }

        String from = ConfigReader.getSenderMail();//отправитель
        String host = ConfigReader.getSmtpServer();//smtp сервер
        String port = ConfigReader.getSmtpPort();

        Properties propertiesM = System.getProperties();
        propertiesM.setProperty("mail.smtp.host",host);
        propertiesM.setProperty("mail.smtp.port",port);
        Session session = Session.getDefaultInstance(propertiesM);
        try {
            MimeMessage message = new MimeMessage(session);//сообщение электронной почты
            message.setFrom(new InternetAddress(from)); //настройка полей заголовка
            message.addRecipients(Message.RecipientType.TO, receiverList);
            message.setSubject(ConfigReader.getSubjectLine()+" Отчеты о звонках"); //тема сообщения

            Multipart multipart = new MimeMultipart();
            MimeBodyPart textBodyPart = new MimeBodyPart();
            textBodyPart.setText("Во вложенном файле отчеты о звонках вашей автоматической телефонной станции.\n"+
                    "Справочник внутренних телефонов можно поправить на компьютере где установлен анализаотор логов: "+ConfigReader.getSprFXSfile()+"\n"+
                    "Справочник регулярных направлений звонков, для автоподстановки, здесь: "+ConfigReader.getSprOutCallFile()+"\n"+
                    "Страничка программы в интернете: http://lkservis.ru/parsercdreltexsmg/");

            MimeBodyPart attachmentBodyPart=new MimeBodyPart();
            //DataSource source = new FileDataSource(zipFilePath);
            FileDataSource source = new FileDataSource(zipFilePath);
            attachmentBodyPart.setDataHandler(new DataHandler(source));
            String s = zipFileName;
            attachmentBodyPart.setFileName(s);



            multipart.addBodyPart(textBodyPart);
            multipart.addBodyPart(attachmentBodyPart);

            message.setContent(multipart);


            //отправка сообщения
            Transport.send(message);
            System.out.println("Cообщение отправлено...");

        }catch (AddressException e){
            e.printStackTrace();
        }catch (MessagingException e){
            e.printStackTrace();
        }

    }
}


package ru.lkservis.parserCdrEltexSmg;
import java.io.*;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
     * данный класс будет создавать текстовые отчеты о звонках в папке ReportDir
     */
    public class ReportCreator {

        private HashMap<String, ArrayList<String>> listParseCdr;
        private MyLogger myLogger;

            public ReportCreator(HashMap<String, ArrayList<String>> listParseCdr) {
                this.listParseCdr = listParseCdr;
                this.myLogger = new MyLogger(ReportCreator.class);
            }

        File ReportDir = new File(ConfigReader.getReportDir());


        //HashMap<String, ArrayList<String>> callMap = logAnalyser.run();
    /**
     * данный метод выводит напрямую в файл из за чего возникают проблемы с кодировкой
     */
        public void reportGenerator(){

            if(!ReportDir.exists()){
                ReportDir.mkdir();
            }else{
                myLogger.print("Проверим есть ли в директории формирования отчетов старые файлы и удалим их");
                try{
                    FileManager fileManager = new FileManager(Paths.get(ReportDir.toString()));
                    List<Path> list = fileManager.getFullPathList();
                    if(list.size()>0){
                        for(Path p:list){
                            myLogger.print("Найден: "+p.getFileName()+" удаляю!");
                            Path delete = p;
                            Files.delete(p);
                        }
                    }
                    myLogger.print("Старые файлы удалены успешно:");

                } catch (IOException e) {
                    myLogger.print("Ошибка удаления старого файла:");
                    myLogger.printStackElements(e.getStackTrace());
                }


            }

            for(Map.Entry<String, ArrayList<String>> entry:listParseCdr.entrySet()){
                BufferedWriter bufferedWriter = null;
                try{
                    //bufferedWriter = new BufferedWriter(new FileWriter(ReportDir.toString()+"\\"+entry.getKey()+".txt"));
                    bufferedWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(ReportDir.toString()+"\\"+entry.getKey()+".txt"),"UTF-8"));
                    String fio = SprFXS.getSprFXS().get(entry.getKey());
                    if(fio==null)fio="________________________";
                    bufferedWriter.write(entry.getKey()+" "+fio);
                    bufferedWriter.newLine();
                    bufferedWriter.newLine();
                    StringBuilder sb = new StringBuilder();//0145
                    for(int i=0;i<entry.getValue().size();i++){
                        bufferedWriter.write(entry.getValue().get(i));
                        bufferedWriter.newLine();
                    }
                    bufferedWriter.newLine();
                    bufferedWriter.newLine();
                    bufferedWriter.newLine();
                    bufferedWriter.write(" ________________________________"+fio);
                    bufferedWriter.flush();
                    bufferedWriter.close();
                }catch (IOException e){
                    e.printStackTrace();
                }finally {
                    try{
                        bufferedWriter.close();
                    }catch (IOException e){
                        e.printStackTrace();
                    }
                }


                //---тестовая печать!
//                System.out.println(entry.getKey()+" "+SprFXS.getSprFXS().get(entry.getKey()));
//                StringBuilder sb = new StringBuilder();//0145
//                for(int i=0;i<entry.getValue().size();i++){
//                    System.out.println(entry.getValue().get(i));
//                }
            }
        }

    /**
     * метод будет генерить строки отчета и писать их в список. а потом уже из списка выводить в файл
     * это должно помочь решить проблему кодировок
     */
    public void reportListGenerator() {

        if (!ReportDir.exists()) {
            ReportDir.mkdir();
        }else{
            myLogger.print("Проверим есть ли в директории формирования отчетов старые файлы и удалим их");
            try{
                FileManager fileManager = new FileManager(Paths.get(ReportDir.toString()));
                List<Path> list = fileManager.getFullPathList();
                if(list.size()>0){
                    for(Path p:list){
                        myLogger.print("Найден: "+p.getFileName()+" удаляю!");
                        Path delete = p;
                        Files.delete(p);
                    }
                }
                myLogger.print("Старые файлы удалены успешно:");

            } catch (IOException e) {
                myLogger.print("Ошибка удаления старого файла:");
                myLogger.printStackElements(e.getStackTrace());
            }

        }
        //String reportName = null
        ArrayList<String> reportList = new ArrayList<>();

        for (Map.Entry<String, ArrayList<String>> entry : listParseCdr.entrySet()) {
            StringBuilder stringBuilder = new StringBuilder();
            String fio = SprFXS.getSprFXS().get(entry.getKey());
            if(fio==null)fio="________________________";
            stringBuilder.append(entry.getKey() + " " + fio);
            stringBuilder.append(System.getProperty("line.separator"));
            stringBuilder.append(System.getProperty("line.separator"));
            StringBuilder sb = new StringBuilder();//0145
            for (int i = 0; i < entry.getValue().size(); i++) {
                stringBuilder.append(entry.getValue().get(i));
                stringBuilder.append(System.getProperty("line.separator"));
            }
            stringBuilder.append(System.getProperty("line.separator"));
            stringBuilder.append(System.getProperty("line.separator"));
            stringBuilder.append(System.getProperty("line.separator"));
            stringBuilder.append(" ________________________________" + fio);


            BufferedWriter bufferedWriter = null;
            try {
                //bufferedWriter = new BufferedWriter(new FileWriter(ReportDir.toString()+"\\"+entry.getKey()+".txt"));
                bufferedWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(ReportDir.toString() + "\\" + entry.getKey() + ".txt"), "UTF-8"));
                //System.out.println("Распечатаем список с отчетами: ");


                    bufferedWriter.write(stringBuilder.toString());
                bufferedWriter.flush();
                bufferedWriter.close();
            } catch (FileNotFoundException e) {
                myLogger.print("Ошибка создания архива - результирующий файл неожиданно стал недоступен:");
                myLogger.printStackElements(e.getStackTrace());
            } catch (UnsupportedEncodingException e) {
                myLogger.print("Ошибка создания файла в кодировке UTF-8, кодировка не поддерживается в данном окружении");
                myLogger.printStackElements(e.getStackTrace());
            } catch (IOException e) {
                myLogger.print("Ошибка ввода\\вывода при записи в файл((");
                myLogger.printStackElements(e.getStackTrace());
            }

        }


    }

    }

package ru.lkservis.parserCdrEltexSmg;



import java.io.*;
import java.util.Properties;

    public class ConfigReader {
        private MyLogger myLogger = new MyLogger(ConfigReader.class);
        private File file;
        private static Properties properties;

        public ConfigReader(File file) throws IOException {
            properties = new Properties();
            properties.loadFromXML(new FileInputStream(file));
        }

       public static String getCdrDir(){
            return properties.getProperty("cdrdir");
       }

       public static int getSaveTimeDay(){
            return Integer.parseInt(properties.getProperty("savetimeday"));
       }

       public static String getReportDir(){
            return properties.getProperty("reportdir");
       }

       public static String getSprFXSfile(){
            return properties.getProperty("sprfxsfile");
       }

       public static String getSprOutCallFile(){
            return properties.getProperty("sproutfile");
       }
       public static String getOutputDir(){
            return properties.getProperty("outputdir");
       }

       public static String getArchiveNamePrefix(){
            return properties.getProperty("archivenameprefix");
       }
       public static String getReceiverMail(){
            return properties.getProperty("receivermail");
       }
       public static String getSenderMail(){
            return properties.getProperty("sendermail");
       }
        public static String getSmtpServer(){
            return properties.getProperty("smtpserver");
        }
        public static String getSmtpPort(){
            return properties.getProperty("smtpport");
        }
        public static String getSubjectLine(){
            return properties.getProperty("subjectline");
        }
        public static String getPrefixForProcFile(){
            return properties.getProperty("prefixforprocfile");
        }
        public static long getSaveArchiveDay(){
            return Long.parseLong(properties.getProperty("savearchiveday"));
        }

    }



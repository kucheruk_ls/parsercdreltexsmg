package ru.lkservis.parserCdrEltexSmg;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * я слежу за тем что бы лог не был слишком большим и что бы обработанные файлы не захламляли директорию
 */
public class LogAndFileCleaner {
    private MyLogger myLogger = new MyLogger(LogAndFileCleaner.class);



    public void logPack(){
        Path logfile = Paths.get("parsercdreltexsmg.log");
        if(logfile.toFile().length()>5242880){
            ArrayList<String> listtmp = new ArrayList<>();
            //try(BufferedReader bufferedReader = new BufferedReader(new FileReader(logfile.toFile()))){
            try(BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(logfile.toFile()),"WINDOWS-1251"))){
                String line = "";

                while ((line=bufferedReader.readLine())!=null){
                    listtmp.add(line);
                }
                bufferedReader.close();
            }catch (FileNotFoundException e){
                myLogger.print(this.getClass().getSimpleName()+": "+"логфайл не найден");
            }catch (IOException e){
                myLogger.print(this.getClass().getSimpleName()+": "+"ошибка Ввода-Вывода при попытке обслуживания(чтение) лог файла");
            }

            try(BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(logfile.toFile()))){
                for(int i=listtmp.size()-1;i>listtmp.size()-100;i--){
                    bufferedWriter.write(listtmp.get(i));
                    bufferedWriter.write(System.getProperty("line.separator"));
                }
                bufferedWriter.flush();
                bufferedWriter.close();

            }catch (IOException e){
                myLogger.print(this.getClass().getSimpleName()+": "+"ошибка Ввода-Вывода при попытке обслуживания(запись) лог файла");
            }
        }
    }

    /**
     * получим список файлов от FileManager и удалим те которые старее filedeleteday
     */
    public void oldFileDelete(List<Path> files, ConfigReader configReader){
        myLogger.print("Проверим есть ли старые архивы(параметр savearchiveday в config.xml) если есть - удалим!");
        List<Path> listDontOld = new ArrayList<>();
        for(Path s:files){
            Date t = new Date();
            Date lm = new Date(s.toFile().lastModified());
            long oldd = (t.getTime()-lm.getTime())/1000/60/60/24;
            if(oldd>ConfigReader.getSaveArchiveDay()){
                try{
                    Files.delete(s);
                    myLogger.print("Файл архива "+s+" удален");
                }catch (IOException e){
                    myLogger.print("Ошибка удаления файла архива");
                    myLogger.printStackElements(e.getStackTrace());
                }
            }
        }

    }


}


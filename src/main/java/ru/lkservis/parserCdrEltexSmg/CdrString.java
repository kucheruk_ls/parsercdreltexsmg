package ru.lkservis.parserCdrEltexSmg;

import java.text.SimpleDateFormat;
import java.util.Date;

public class CdrString {
    private MyLogger myLogger = new MyLogger(CdrString.class);
    private String indPriznak;//отличительный признак
    private Date responseTime;//время ответа на вызов
    private double callDuration;//длительность вызова
    private String reasonDisconnect;//причина зазъединения
    private String statusCallDisconnect;//статус вызова при разъединении
    private String callerIp;//ip адрес вызывающего
    private String callerTipe;//тип вызывающего
    private String callerSubscribe;//описание вызывающего
    private String callerInputNumber;//входящий номер вызывающего
    private String callerOutputNumber;//исходящий номер вызывающего
    private String calledIp;//ip адрес вызываемого
    private String calledTipe;//тип вызываемого
    private String calledSubscribe;//описание вызываемого
    private String calledInputNumber;//входящий номер вызываемого
    private String calledOutputNumber;//исходящий номер вызываемого
    private Date callArrivalTime;//время поступления вызова
    private Date callDisconnectTime;//время разъединения вызова

    public CdrString(String indPriznak, Date responseTime, double callDuration, String reasonDisconnect, String statusCallDisconnect, String callerIp, String callerTipe, String callerSubscribe, String callerInputNumber, String callerOutputNumber, String calledIp, String calledTipe, String calledSubscribe, String calledInputNumber, String calledOutputNumber, Date callArrivalTime, Date callDisconnectTime) {
        this.indPriznak = indPriznak;
        this.responseTime = responseTime;
        this.callDuration = callDuration;
        this.reasonDisconnect = reasonDisconnect;
        this.statusCallDisconnect = statusCallDisconnect;
        this.callerIp = callerIp;
        this.callerTipe = callerTipe;
        this.callerSubscribe = callerSubscribe;
        this.callerInputNumber = callerInputNumber;
        this.callerOutputNumber = callerOutputNumber;
        this.calledIp = calledIp;
        this.calledTipe = calledTipe;
        this.calledSubscribe = calledSubscribe;
        this.calledInputNumber = calledInputNumber;
        this.calledOutputNumber = calledOutputNumber;
        this.callArrivalTime = callArrivalTime;
        this.callDisconnectTime = callDisconnectTime;
    }

    public MyLogger getMyLogger() {
        return myLogger;
    }

    public String getIndPriznak() {
        return indPriznak;
    }

    public Date getResponseTime() {
        return responseTime;
    }

    public double getCallDuration() {
        return callDuration;
    }

    public String getReasonDisconnect() {
        return reasonDisconnect;
    }

    public String getStatusCallDisconnect() {
        return statusCallDisconnect;
    }

    public String getCallerIp() {
        return callerIp;
    }

    public String getCallerTipe() {
        return callerTipe;
    }

    public String getCallerSubscribe() {
        return callerSubscribe;
    }

    public String getCallerInputNumber() {
        return callerInputNumber;
    }

    public String getCallerOutputNumber() {
        return callerOutputNumber;
    }

    public String getCalledIp() {
        return calledIp;
    }

    public String getCalledTipe() {
        return calledTipe;
    }

    public String getCalledSubscribe() {
        return calledSubscribe;
    }

    public String getCalledInputNumber() {
        return calledInputNumber;
    }

    public String getCalledOutputNumber() {
        return calledOutputNumber;
    }

    public Date getCallArrivalTime() {
        return callArrivalTime;
    }

    public Date getCallDisconnectTime() {
        return callDisconnectTime;
    }

    @Override
    public String toString() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return "CdrString{" +
                "myLogger=" + myLogger +
                ", indPriznak='" + indPriznak + '\'' +
                ", responseTime=" + simpleDateFormat.format(responseTime) +
                ", callDuration=" + callDuration +
                ", reasonDisconnect='" + reasonDisconnect + '\'' +
                ", statusCallDisconnect='" + statusCallDisconnect + '\'' +
                ", callerIp='" + callerIp + '\'' +
                ", callerTipe='" + callerTipe + '\'' +
                ", callerSubscribe='" + callerSubscribe + '\'' +
                ", callerInputNumber='" + callerInputNumber + '\'' +
                ", callerOutputNumber='" + callerOutputNumber + '\'' +
                ", calledIp='" + calledIp + '\'' +
                ", calledTipe='" + calledTipe + '\'' +
                ", calledSubscribe='" + calledSubscribe + '\'' +
                ", calledInputNumber='" + calledInputNumber + '\'' +
                ", calledOutputNumber='" + calledOutputNumber + '\'' +
                ", callArrivalTime=" + simpleDateFormat.format(callArrivalTime) +
                ", callDisconnectTime=" + simpleDateFormat.format(callDisconnectTime) +
                '}';
    }
}

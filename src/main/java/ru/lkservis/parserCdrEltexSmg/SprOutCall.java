package ru.lkservis.parserCdrEltexSmg;

import java.io.*;
import java.util.HashMap;

/**
 * класс возвращает список известных адресатов звонков
 */
public class SprOutCall {
    static File spr = new File(ConfigReader.getSprOutCallFile());

    public static HashMap<String, String> getSprOutCall(){
        HashMap<String, String> sprMap = new HashMap<>();
        try{
            //BufferedReader bufferedReader = new BufferedReader(new FileReader(spr.toString()));
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(spr),"UTF-8"));
            String str;
            while ((str=bufferedReader.readLine())!=null) {
                if(!str.startsWith("#")) {
                    String[] tmp = str.split(" ");
                    sprMap.put(tmp[0], tmp[1]);
                }
            }
            bufferedReader.close();
        }catch (FileNotFoundException e){
            e.printStackTrace();
        }catch (IOException e){
            e.printStackTrace();
        }
        return sprMap;
    }
}

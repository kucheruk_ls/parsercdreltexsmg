package ru.lkservis.parserCdrEltexSmg;

import java.nio.file.Path;
import java.util.ArrayList;

public interface FileCommander {
    /**
     * метод получает в качестве параметра список файлов в директории обработки от класса CdrParser и переименовывает эти файлы
     * добавляя впереди файла префикс вида done_текущая дата по шаблону YYYY:MM:dd HH:mm:ss
     * данный метод запускается после выборки данных из файлов и помечает файлы данные из которых уже выбраны
     * @param fileDirList
     */
    public void FileRename(ArrayList<Path> fileDirList);

    /**
     * данный метод удаляет файлы с истекшим сроком хранения. Срок хранения задается в config.xml
     * @param fileDirList
     * @param saveTimeDay
     */
    public void FileDeleter(ArrayList<Path> fileDirList, int saveTimeDay);
}
